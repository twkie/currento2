using Toybox.WatchUi;
using Toybox.Application.Properties;
import Toybox.Lang;

class CurrentO2SettingsMenu extends WatchUi.Menu2 {

    function initialize() {
        Menu2.initialize(null);
        Menu2.setTitle("Settings");
        Menu2.addItem(new WatchUi.MenuItem("Min SPO2", null, "minO2", null));
        Menu2.addItem(new WatchUi.MenuItem("Vibrate", null, "vibrate", null));
    }
}

class CurrentO2SettingsMenuDelegate extends WatchUi.Menu2InputDelegate {

    function initialize() {
        Menu2InputDelegate.initialize();
    }

    function onSelect(menuItem) {
        if (menuItem.getId().equals("minO2")) {
            var minSPO2 = Properties.getValue("minSPO2") as Numeric;
            var spo2Picker = new NumberPicker("Min SPO2", new NumberFactory(50, 100, 1, null), minSPO2);

            WatchUi.pushView(
                spo2Picker,
                new MinO2PickerDelegate(spo2Picker),
                WatchUi.SLIDE_IMMEDIATE
            );
        } else if (menuItem.getId().equals("vibrate")) {
            var vibrate = Properties.getValue("vibrate") as Numeric;

            var vibratePicker = new NumberPicker("Vibrate %", new NumberFactory(0,100,25, null), vibrate);

            WatchUi.pushView(
                vibratePicker,
                new VibratePickerDelegate(vibratePicker),
                WatchUi.SLIDE_IMMEDIATE
            );
        }
    }
}