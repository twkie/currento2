using Toybox.Application;
using Toybox.Graphics;
using Toybox.WatchUi;
using Toybox.Application.Properties;
import Toybox.Lang;

class NumberPicker extends WatchUi.Picker {
    hidden var mTitleText;
    hidden var mFactory;

    function initialize(mTitleText as String, mFactory as NumberFactory, initial as Numeric) {
        var startingPoint = mFactory.getIndex(initial);


        mTitle = new WatchUi.Text({:text=>mTitleText, :locX =>WatchUi.LAYOUT_HALIGN_CENTER, :locY=>WatchUi.LAYOUT_VALIGN_BOTTOM, :color=>Graphics.COLOR_WHITE});

        Picker.initialize({:title=>mTitle, :pattern=>[mFactory], :defaults=>[startingPoint]});
    }

    function onUpdate(dc) {
        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();
        Picker.onUpdate(dc);
    }

    function getTitle() {
        return mTitleText.toString();
    }

    function getTitleLength() {
        return mTitleText.length();
    }

    function isDone(value) {
        return mFactory.isDone(value);
    }
}

class NumberPickerDelegate extends WatchUi.PickerDelegate {
    hidden var mPicker;

    function initialize(picker) {
        PickerDelegate.initialize();
        mPicker = picker;
    }

    function onCancel() {
        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        return true;
    }

    function onAccept(values) {
        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        return true;
    }

}

class MinO2PickerDelegate extends NumberPickerDelegate {
    function initialize(picker) {
        NumberPickerDelegate.initialize(picker);
    }

    function onAccept(values) {
        Properties.setValue("minSPO2", values[0]);
        NumberPickerDelegate.onAccept(values);
    }
}

class VibratePickerDelegate extends NumberPickerDelegate {
    function initialize(picker) {
        NumberPickerDelegate.initialize(picker);
    }

    function onAccept(values) {
        Properties.setValue("vibrate", values[0]);
        return NumberPickerDelegate.onAccept(values);
    }
}
