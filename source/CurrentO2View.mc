import Toybox.Activity;
import Toybox.Lang;
import Toybox.Time;
import Toybox.WatchUi;
import Toybox.Attention;
import Toybox.Application.Properties;

class DataFieldAlertView extends WatchUi.DataFieldAlert{

    hidden var curSPO2 as Numeric;

    function initialize(currentOxygen as Numeric) {
        DataFieldAlert.initialize();
        curSPO2 = currentOxygen;

        if (Attention has :vibrate) {
            var vibrate = Properties.getValue("vibrate");
            Attention.vibrate([new Attention.VibeProfile(vibrate, 2000)]);
        }

        if (Attention has :backlight) {
            Attention.backlight(true);
        }
    }

    function onUpdate(dc){
        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);

        dc.drawText(dc.getWidth() / 2, dc.getHeight() / 3, Graphics.FONT_LARGE, "Low O2 Alert", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
        dc.drawText(dc.getWidth() / 2, dc.getHeight() / 1.5, Graphics.FONT_NUMBER_THAI_HOT, curSPO2.format("%d"), Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
    }
}

class CurrentO2View extends WatchUi.SimpleDataField {

    hidden var minO2 as Numeric;
    hidden var lastAlertTime as Numeric;
    hidden var alertBackoffSeconds as Numeric;

    // Set the label of the data field here.
    function initialize() {
        SimpleDataField.initialize();
        label = "SPO2";
        minO2 = Properties.getValue("minSPO2");
        alertBackoffSeconds = Properties.getValue("alertBackoffSeconds");
        lastAlertTime = 0;
    }

    function compute(info as Activity.Info) {
        var oxygenSaturation = info.currentOxygenSaturation;
        if (!(info has :currentOxygenSaturation) || oxygenSaturation == null) {
            return null;
        }

        // Setup alerting
       if ((WatchUi.DataField has :showAlert) && minO2 != null) {
            var timeNow = Time.now().value();
            if (oxygenSaturation < minO2 && (lastAlertTime == 0 || timeNow > (lastAlertTime + 45))) {
                lastAlertTime = timeNow;
                WatchUi.DataField.showAlert(new DataFieldAlertView(oxygenSaturation));
            }
        }

        return info.currentOxygenSaturation;
    }
}